package com.example.tap_game

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var countDownTimer : CountDownTimer
    private var initialCountDownTimer: Long = 10000
    private var countDownInterval: Long = 1000

    private lateinit var title: TextView
    private lateinit var txtRandomValue: TextView
    private lateinit var tapButton: Button
    private lateinit var startButton: Button

    private var timeLeft = 10
    private var gameScore = 0
    private var isGameStarted = false
    private var n = (0..10).shuffled().first()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        title = findViewById(R.id.txt_title)
        tapButton = findViewById(R.id.btn_tap)
        startButton = findViewById(R.id.btn_Start)
        txtRandomValue = findViewById(R.id.txt_random)

        tapButton.setOnClickListener{startGame()}

        if(!isGameStarted){
            restoreGame()
        }else{
            resetGame()
        }
    }



    private fun configCountDownTimer(){
        countDownTimer = object : CountDownTimer((timeLeft * 1000).toLong(), countDownInterval){
            override fun onTick(millisUnit: Long) {
                timeLeft = millisUnit.toInt() / 1000
                //timeLeftTextView.text = getString(R.string.time_label, timeLeft)
            }

            override fun onFinish() {
                endGame()
            }
        }
    }

    private fun endGame(){

        if(timeLeft == n){
            gameScore = 100
        }else if(timeLeft == (n - 1) || timeLeft == (n + 1)){
            gameScore = 50
        }
        Toast.makeText(this, getString(R.string.game_over, gameScore), Toast.LENGTH_SHORT).show()
        resetGame()
    }

    private fun startGame(){
        val newNumber = n
        countDownTimer.start()
        isGameStarted = true
        txtRandomValue.text = getString(R.string.txt_randomValue, newNumber)



    }

    private fun resetGame(){
        gameScore = 0
        //gameScoreTextView.text = getString(R.string.score_label, gameScore)

        timeLeft = 10
        //timeLeftTextView.text = getString(R.string.time_label, timeLeft)

        countDownTimer = object : CountDownTimer(initialCountDownTimer, countDownInterval){
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                //timeLeftTextView.text = getString(R.string.time_label, timeLeft)
            }

        }
        isGameStarted = false
    }

    private fun restoreGame(){
        //gameScoreTextView.text = getString(R.string.score_label, gameScore)
        //timeLeftTextView.text = getString(R.string.time_label, timeLeft)

        countDownTimer = object : CountDownTimer((timeLeft * 1000).toLong(), countDownInterval){
            override fun onTick(millisUnit: Long) {
                timeLeft = millisUnit.toInt() / 1000
                //timeLeftTextView.text = getString(R.string.time_label, timeLeft)
            }

            override fun onFinish() {
                endGame()
            }
        }

        if(isGameStarted){
            countDownTimer.start()
        }
    }

}
